Please read before accessing this project repo.

This README will contain instructions to operate the prototype website in a virtual enviroment.

Link to project repo (https://gitlab.com/ARirwin47/recipe-website-project)

1. Download the zip file (If using moodle) and unzip the folder into any area on your computer. (Using either winRAR or 7Zip)
Additionaly, you can clone the gitlab folder using this guide (https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html). 

2. Please ensure that docker is installed on your desktop computer before running this website on a local server. This website will not run without docker!

3. Once docker is running on your desktop, open up Windows PowerShell by right clicking on the start button and selecting "Windows PowerShell (admin)" in the dropdown menu (NOT the main Start menu!)

4. In PowerShell, change your windows directory to the folder where the project folder is stored 
(For example: cd C:\Users\CompSci\Desktop\Coursework\recipe) 

5. To build the website in docker,type in "docker-compose up --build" (Without the quote marks!) in PowerShell. A section should appear in docker called "recipe"

6. In docker, click on the dropdown arrow next to recipe and you should see two sections. Once called recipe and one called recipe-MySQL. In the recipe section, click on the button called "Open in browser" (The button looks like a box with a arrow pointing out of the box). Docker should open the website in the web browser of your choice!