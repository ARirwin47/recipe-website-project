drop database if exists recipe;
create database recipe;

use recipe;
drop table if exists Publishers;

CREATE TABLE Publishers (
   PublisherID int,
   name varchar(255),
   PRIMARY KEY(PublisherID)
);


drop table if exists Recipes;
CREATE TABLE IF NOT EXISTS Recipes (
   RecipeID int NOT NULL,
   PublisherID int,
   title varchar(255),
   link varchar(255),
   year int,
   PRIMARY KEY(RecipeID),
   FOREIGN KEY(PublisherID) REFERENCES Publishers(PublisherID)
);

drop table if exists Authors;
CREATE TABLE Authors (
   AuthorID int NOT NULL,
   initial char(1),
   lastName varchar(1024),
   PRIMARY KEY(AuthorID)
);

drop table if exists RecipesAuthors;
CREATE TABLE RecipesAuthors (
   AuthorID int NOT NULL,
   RecipesID int NOT NULL,
   PRIMARY KEY(AuthorID, RecipesID),
   FOREIGN KEY(AuthorID) REFERENCES Authors(AuthorID),
   FOREIGN KEY(RecipesID) REFERENCES Recipes(RecipeID)
);


INSERT INTO Authors VALUES(0, 'M','Turner');
INSERT INTO Authors VALUES(1, 'C','Havranek');
INSERT INTO Authors VALUES(2, 'J','Cho');
INSERT INTO Authors VALUES(3, 'K', 'Aubin');

INSERT INTO Publishers VALUES (0,"BBC goodfood");
INSERT INTO Publishers VALUES (1,"Simply Recipes");
INSERT INTO Publishers VALUES (2,"Eater");
INSERT INTO Publishers VALUES (3, "Tasty");

INSERT INTO Recipes VALUES (0, 0, 'Chorizo & mozzarella gnocchi bake', 'https://www.bbcgoodfood.com/recipes/chorizo-mozzarella-gnocchi-bake', 2021);
INSERT INTO Recipes VALUES (1, 1, 'Easy Avocado and Black Bean Quesadillas', 'https://www.simplyrecipes.com/recipes/easy_avocado_and_black_bean_quesadillas/', 2021);
INSERT INTO Recipes VALUES (2, 2, 'Almond and Persimmon Cake', 'https://www.eater.com/22816313/almond-persimmon-cake-recipe-joy-cho-almond-flour', 2021);
INSERT INTO Recipes VALUES (3, 3, '20-Minute One-Pan Pizza', 'https://tasty.co/recipe/20-minute-one-pan-pizza', 2021);

INSERT INTO RecipesAuthors VALUES (0,0);
INSERT INTO RecipesAuthors VALUES (1,1);
INSERT INTO RecipesAuthors VALUES (2,2);
INSERT INTO RecipesAuthors VALUES (3,3);
