import mysql.connector
from config import db_config

def get_data():
    return [
        {"title":"Chorizo & mozzarella gnocchi bake.", "author":"Turner, M."},
        {"title":"Easy Avocado and Black Bean Quesadillas", "author":"Havranek, C"},
        {"title":"Almond and Persimmon Cake", "author":"Cho, J."},
        {"title":"20-Minute One-Pan Pizza", "author":"Aubin, K"}
    ]

def get_recipes():
    recipes_data = []
    db = mysql.connector.connect(**db_config)
    cursor = db.cursor()
    cursor.execute("SELECT * FROM Recipes")
    recipes = cursor.fetchall()
    #print (recipes)
    for recipe in recipes:
      author = cursor.fetchall()[0]
      recipe_author = author[1]+","+author[0]+"."
      recipes_data.append({'title': recipe[2], 'author':recipe_author})
      recipe_id = recipe[0]
      author_query = f"SELECT initial, lastName FROM Authors INNER JOIN RecipesAuthors where RecipesAuthors.BookID = {recipe_id} AND Authors.AuthorID = RecipesAuthors.AuthorID";
      cursor.execute(author_query)
      return recipes_data