from flask import Flask, render_template, url_for
from util import get_recipes, get_data

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html', page_name="Home:")

@app.route('/about')
def about():
    return render_template('about.html', page_name="About:")

@app.route('/recipelist')
def recipes():
    return render_template('recipelist.html', page_name="Recipe:", data=get_recipes())

app.run(host="0.0.0.0", debug=True)